var lunr = require('lunr'),
    stdin = process.stdin,
    stdout = process.stdout,
    buffer = []

stdin.resume()
stdin.setEncoding('utf8')

stdin.on('data', function (data) {
  buffer.push(data)
})

stdin.on('end', function () {
  var documents = JSON.parse(buffer.join(''))

  var mapper = function (document) {
    return {
      id: document.id,
      user: document.user.map(a => a.name),
      name: document.name,
      description: document.description,
      keyword: document.keyword
    }
  }

  var idx = lunr(function () {
    this.ref('id')
    this.field('user')
    this.field('name')
    this.field('description')
    this.field('keyword')

    documents.forEach(function (doc) {
      this.add(mapper(doc))
    }, this)
  })

  stdout.write(JSON.stringify(idx))
})
